import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;

public class FoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton udonButton;
    private JButton ramenButton;
    private JButton sobaButton;
    private JButton checkOutButton;
    private JTextPane orderedItemsList;
    private JLabel total;

    public static int sum = 0;
    void order(String foodName,int foodPrice){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+foodName+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + foodName + "  " + foodPrice + "yen\n");
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + foodName + " ! It will be served as soon as possible.");
            sum+=foodPrice;
        }
    }
    void total(int sum){
        total.setText("total: "+sum+"yen");
    }
    public FoodOrderingMachine() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura", 150);
                total(sum);
            }
        });
        tempuraButton.setIcon(new ImageIcon(this.getClass().getResource("tempura.png")));

        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage", 200);
                total(sum);
            }
        });
        karaageButton.setIcon(new ImageIcon(this.getClass().getResource("karaage.jpg")));

        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza", 170);
                total(sum);
            }
        });
        gyozaButton.setIcon(new ImageIcon(this.getClass().getResource("gyoza.png")));

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon", 400);
                total(sum);
            }
        });
        udonButton.setIcon(new ImageIcon(this.getClass().getResource("udon.jpg")));

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", 500);
                total(sum);
            }
        });
        ramenButton.setIcon(new ImageIcon(this.getClass().getResource("ramen.jpg")));

        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba", 450);
                total(sum);
            }
        });
        sobaButton.setIcon(new ImageIcon(this.getClass().getResource("soba.jpg")));

        total.addComponentListener(new ComponentAdapter() {});

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "If you have discount coupon,you can get a 20% discount.\nDo you have discount coupon?",
                        "Discount",
                        JOptionPane.YES_NO_OPTION
                );

                if (confirmation == 0) {
                    int confirmation2 = JOptionPane.showConfirmDialog(null,
                            "Would you like to checkout?",
                            "Checkout Confirmation",
                            JOptionPane.YES_NO_OPTION
                    );
                    if(confirmation2 == 0) {
                        total.setText("total: "+sum*8/10 + "yen");
                        JOptionPane.showMessageDialog(null, "Thank you for using the discount coupon. The total price is " + sum*8/10 + " yen.");
                        orderedItemsList.setText("");
                        total(0);
                        sum = 0;
                    }
                }
                else{
                    int confirmation3 = JOptionPane.showConfirmDialog(null,
                            "Would you like to checkout?",
                            "Checkout Confirmation",
                            JOptionPane.YES_NO_OPTION
                    );
                    if (confirmation3 == 0) {
                        JOptionPane.showMessageDialog(null, "Thank you. The total price is " + sum + " yen.");
                        orderedItemsList.setText("");
                        total.setText("total: "+sum + "yen");
                        total(0);
                        sum = 0;
                    }
                }
            }
        });

    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}